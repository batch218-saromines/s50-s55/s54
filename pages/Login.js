import {useState, useEffect, useContext} from 'react';
import { Form, Button } from 'react-bootstrap';
import {Navigate} from 'react-router-dom';

import UserContext from '../UserContext';



    // Allows us to cosume the User Context Object and properties to use for user validation


export default function Login() {
	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);
   
    const {user, setUser} = useContext(UserContext);

    // hook returns a function that lets us navigate to components
    const navigate = useNavigate()

    // Function to simulate redirection via form submission
    function authenticate(e) {
        // Prevents page redirection via form submission
        e.preventDefault();

        // set the email if the authenticated user in the local storage.
        localStorage.setItem('email', email);
        setUser({email: localStorage.getItem('email')})

        // Clear input fields after submission
        setEmail('');
        setPassword('');
        navigate('/');

        alert(`${email} has been verified! Welcome back!`);

    }

useEffect(() => {
    // Validation to enable submit button when all fields are populated and both passwords match
    if(email !== '' && password !== ''){
        setIsActive(true);
    }else{
        setIsActive(false);
    }
}, [email, password]);


return (
            (user.email !== null) ?
            <Navigate to= "/courses"/>
        :

    <Form onSubmit={(e) => authenticate(e)}>
        <Form.Group controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control 
                type="email" 
                placeholder="Enter email"
                value={email}
        		onChange={(e) => setEmail(e.target.value)} 
                required
            />
        </Form.Group>

        <Form.Group controlId="password">
            <Form.Label>Password</Form.Label>
            <Form.Control 
                type="password" 
                placeholder="Password" 
                value={password}
        		onChange={(e) => setPassword(e.target.value)}
                required
            />
        </Form.Group>

         { isActive ? 
        <Button variant="primary" type="submit" id="submitBtn">
            Submit
        </Button>
        : 
        <Button variant="danger" type="submit" id="submitBtn" disabled>
            Submit
        </Button>
    }
    </Form>
    )
}